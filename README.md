# Putting Active Categories

This Lambda function, meant for use in the DynamoDB on AWS, takes one camera ID at a time and a dictionary containing all of the category settings that should be included or removed.
Note that by default, all of the existing active categories in a camera before calling upon this function **will remain the same, unless you explicitly
update this through this function to remove the active category.** This function also will only remove categories if and only if they were previously enabled.

A sample input is as follows:

```
{
  "camera_id": "TEST12-B_CAM1",
  "categories_settings": {
    "cat": false,
    "car": true,
    "person": true
  }
}
```

Here, we update the camera labeled as TEST1B-B_CAM1 to remove any potential detection for cats and enable detection for cars and people, setting both to be vacant for now. 

