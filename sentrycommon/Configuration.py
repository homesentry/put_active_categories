import os
import logging
logger = logging.getLogger(__name__)

AWS_ACCESS_KEY = os.getenv('aws_access_key', "No key set")
AWS_SECRET_KEY = os.getenv('aws_secret_key', "No key set")

TWILIO_SID = "AC33167fe87762b10b77fb0e80051186fa"
TWILIO_TOKEN = "a155d59c9223209e70c3b7b42a5fa8c9"


IMAGES_BUCKET_NAME = os.getenv('sentryImagesBucket', "No bucket set")
SQS_QUEUE_NAME = os.getenv('SQS_Queue', "No SQS_Queue set")
PEOPLE_RESULT_HANDLER_LAMBDA_NAME = os.getenv('RESULT_LAMBDA', "No RESULT_LAMBDA set")
TWILIO_LAMBDA_NAME = os.getenv('call_twilio', "No call_twilio set")
STATIC_BBX_LAMBDA_NAME = os.getenv('static_bbx_lambda', 'No static lambda set')
TWILIO_IMAGE_BUCKET_NAME = 'sentry-twilio-images'


# Dynamo Globals
CAMERA_TABLE_NAME = os.getenv('CAMERA_TABLE_NAME', "Cameras")
IMAGE_TABLE_NAME = "Images"
CUSTOMER_TABLE_NAME = "Customer"
REGION = "us-west-2"
MARKETING_TABLE_NAME = "MarketingMessageUpdate"
BETA_CUSTOMER = "BetaCustomers"


# Camera Table Codes
CAMERA_CODE = "camera_id"
CAMERA_NAME_CODE = "camera_name"
OCCUPIED_CODE = "occupied"
PHONE_CODE = "phone_number"
TIME_CODE = "last_occupied_time"
CAMERA_TYPE_CODE = "camera_type"
LOCATION_CODE = "camera_location"
DEV_PROD_CODE = "dev_or_prod"
REGIONS_TO_IGNORE = "regions_to_ignore"
STATIC_BOUNDING_BOX = "static_bounding_box"
STATIC_UPDATE_TIME = "last_static_bbx_update_time"

SENTRY_ID = 'sentry_id'


# Image Table Codes
IMAGE_CODE = "image_id"
REC_BUCKET_CODE = "received_bucket"
REC_KEY_CODE = "received_key"
RESULTS_CODE = "cnn_result_code"
ALERT_SENT_CODE = "alert_sent"
TIME_STAMP_CODE = "time_stamp"
FINAL_PEOPLE_BOXES = "final_people_boxes"

# Temporal Analysis
DAYS_BEFORE_CURRENT_TIME = 7    # How many days before the current time to check activity
TIME_WINDOW = 60                # Checking for activity in a 60 minute window, i.e. +/- 30 minutes seems reasonable
ACTIVITY_THRESHOLD = 10         # If there is 10 events during window, let's say this is typically an active time in the household

# Admin portal globals
IMAGE_DIR = "drawing_images"       # used by views.py
