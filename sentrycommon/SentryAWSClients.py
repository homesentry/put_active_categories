from sentrycommon import Configuration as conf
import logging
import boto3
from decimal import Decimal
import traceback
import time
import datetime
from boto3.dynamodb.conditions import Key

"""
Classes designed to assist redis access in our application
"""


class SentryDynamo:
    def __init__(self):
        self.dynamo_client = boto3.resource('dynamodb',
                                            aws_access_key_id=conf.AWS_ACCESS_KEY,
                                            aws_secret_access_key=conf.AWS_SECRET_KEY,
                                            region_name=conf.REGION)
        self.dynamo_low_level_client = boto3.client('dynamodb',
                                                    aws_access_key_id=conf.AWS_ACCESS_KEY,
                                                    aws_secret_access_key=conf.AWS_SECRET_KEY,
                                                    region_name=conf.REGION)
        self.camera_table = self.dynamo_client.Table(conf.CAMERA_TABLE_NAME)
        self.customer_table = self.dynamo_client.Table(conf.CUSTOMER_TABLE_NAME)
        self.logger = logging.getLogger(__name__)
        self.logger.info("CAMERA_TABLE_NAME = {0}".format(conf.CAMERA_TABLE_NAME))
        self.camera_table_scan = None
        self.marketing_table = self.dynamo_client.Table(conf.MARKETING_TABLE_NAME)
        self.beta_customer = self.dynamo_client.Table(conf.BETA_CUSTOMER)

    def get_beta_customers(self):
        table_data = []
        try:
            table = self.beta_customer
            response = table.scan()
            table_data = response['Items']
            while 'LastEvaluatedKey' in response:
                response = table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey']
                )
                table_data.extend(response['Items'])
        except Exception as ex:
            self.logger.error('Table scan failed '+ str(ex))
        return table_data


    def get_cameras_properties_for_sentry_id(self, sentry_id):
        cameras = []

        self.scan_camera_table()

        for camera in self.camera_table_scan:
            if sentry_id in camera['camera_id']:
                cameras.append(camera)

        return cameras
        
        

    def scan_camera_table(self):
        if self.camera_table_scan is None:
            self.camera_table_scan = self.scan_table(self.camera_table)
        return self.camera_table_scan

    def scan_table(self, dynamo_table):
        table_data = None
        try:
            response = dynamo_table.scan()
            table_data = response['Items']
            while 'LastEvaluatedKey' in response:
                response = dynamo_table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey']
                )
                table_data.extend(response['Items'])
        except Exception as ex:
            self.logger.error('Table scan failed ' + str(ex))

        return table_data

    def set_camera_properties(self, camera_id, properties_to_update):
        self.__set_table_properties(self.camera_table, conf.CAMERA_CODE, camera_id, properties_to_update)

    def get_camera_properties(self, camera_id):
        return self.__get_item(self.camera_table, conf.CAMERA_CODE, camera_id)

    def set_customer_properties(self, sentry_id, properties_to_update):
        self.__set_table_properties(self.customer_table, 'sentry_id', sentry_id, properties_to_update)

    def get_customer_properties(self, sentry_id):
        return self.__get_item(self.customer_table, conf.SENTRY_ID, sentry_id)

    def delete_customer(self, customer_id):
        self.__delete_item(self.customer_table, conf.SENTRY_ID, customer_id)

    def get_phone_numbers_from_camera_id(self, camera_id):
        camera_properties = self.get_camera_properties(camera_id)

        phone_number = camera_properties[conf.PHONE_CODE]
        phone_number = '+' + phone_number
        return phone_number

    def set_image_properties(self, image_id, properties_to_update):
        camera_id, timestamp = image_id.split("@")
        timestamp = float(timestamp)
        table = self.get_correct_table(timestamp)
        self.__set_table_properties(table, conf.CAMERA_CODE, camera_id, properties_to_update,
                                    range_key_name=conf.TIME_STAMP_CODE, range_key_value=timestamp)

    def get_image_properties(self, image_id):
        camera_id, timestamp = image_id.split("@")
        timestamp = float(timestamp)
        return self.__get_item(self.get_correct_table(timestamp), conf.CAMERA_CODE, camera_id,
                               range_key_name=conf.TIME_STAMP_CODE,
                               range_key_value=timestamp)

    def get_images_between_time(self, camera_id, minimum_time, maximum_time):
        images = []
        index_time = minimum_time
        while index_time < maximum_time + 24 * 60 * 60:
            response = self.__get_entries_with_hash_and_range_key(
                self.get_correct_table_name(index_time),
                conf.CAMERA_CODE, camera_id,
                conf.TIME_STAMP_CODE, minimum_time, maximum_time,
            )
            images.extend(response)
            index_time += 24 * 60 * 60  # Get the next day if we need it.

        return images

    # Each table represents a day of images.
    # search_time will therefore get all the images in the table associated with search_time
    def get_images_for_day(self, search_time):
        images = []

        table = self.dynamo_client.Table(self.get_correct_table_name(search_time))
        try:
            response = table.scan()
            self.process_response(response, images)

            while 'LastEvaluatedKey' in response:
                response = table.scan(
                    ExclusiveStartKey=response['LastEvaluatedKey']
                )
                self.process_response(response, images)

        except Exception as ex:
            self.logger.error('Table scan failed. ' + str(ex))
            return []

        return images

    # Each table represents a day of images.
    # search_time will therefore get all the images in the table associated with search_time
    def get_images_for_camera_for_day(self, camera_id, date):
        images = []

        table = self.dynamo_client.Table(self.get_correct_table_name(date))
        try:
            response = table.query(KeyConditionExpression=Key(conf.CAMERA_CODE).eq(camera_id))
            self.process_response(response, images)

            while 'LastEvaluatedKey' in response:
                response = table.query(KeyConditionExpression=Key(conf.CAMERA_CODE).eq(camera_id), ExclusiveStartKey=response['LastEvaluatedKey'])
                self.process_response(response, images)

        except Exception as ex:
            self.logger.error('Table scan failed. ' + str(ex))
            return []

        return images

    def process_response(self, response, images):
        for image in response['Items']:
            converted_dict = self.decimal_to_float_dictionary(image)
            images.append(converted_dict)

    def delete_camera(self, camera_id):
        self.__delete_item(self.camera_table, conf.CAMERA_CODE, camera_id)

    def __delete_item(self, table, hash_key, hash_value, range_key_name=None, range_key_value=None):
        table.delete_item(Key=self.__get_key_dictionary(hash_key, hash_value, range_key_name, range_key_value))

    def __get_item(self, table, hash_key, hash_key_value, range_key_name=None, range_key_value=None):
        returned = table.get_item(Key=self.__get_key_dictionary(hash_key,
                                                                hash_key_value,
                                                                range_key_name,
                                                                range_key_value))
        if 'Item' not in returned.keys():
            self.logger.error('Item ' + hash_key + ": " + hash_key_value + ' not found in table ' + str(table))
            return None
        returned = returned["Item"]
        return self.decimal_to_float_dictionary(returned)

    def __set_table_properties(self, table, hash_key, hash_key_value, properties_to_update, range_key_name=None,
                               range_key_value=None):
        update_expression = "set"
        if len(properties_to_update.items()) == 0:
            return
        expression_attribute_values = {}

        properties_to_update = self.__float_to_decimal_dictionary(properties_to_update)
        for count, item in enumerate(properties_to_update.items()):
            key, value = item
            update_expression += " {0}=:{1},".format(key, count)
            expression_attribute_values[":{0}".format(count)] = value

        key_dict = self.__get_key_dictionary(hash_key, hash_key_value, range_key_name, range_key_value)

        update_expression = update_expression[:-1]

        response = table.update_item(
            Key=key_dict,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=expression_attribute_values,
            ReturnValues="UPDATED_NEW"
        )
        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            self.logger.error("Failed to update values for table: {0}".format(table))

    def __get_key_dictionary(self, hash_key_name, hash_key_value, range_key_name, range_key_value):
        if type(hash_key_value) == float:
            hash_key_value = Decimal(hash_key_value)
        if type(range_key_value) == float:
            range_key_value = Decimal(range_key_value)

        key_dict = {hash_key_name: hash_key_value}
        if range_key_name is not None or range_key_value is not None:
            if range_key_name is None or range_key_value is None:
                self.logger.warning(
                    "Provided range key: {0}, value: {1} invalid".format(range_key_name, range_key_value))
                return
            else:
                key_dict[range_key_name] = range_key_value
        return key_dict

    def get_correct_table(self, timestamp):
        table_name = self.get_correct_table_name(timestamp)
        return self.dynamo_client.Table(table_name)

    def get_paginated_table_name_list(self, timestamp):
        # list_tables returns a paginated list of 100, use yesterday's table to look for today's
        yesterdays_table_name = self.get_correct_table_name(timestamp - 24 * 3600)
        tables_list = self.dynamo_low_level_client.list_tables(ExclusiveStartTableName=yesterdays_table_name)
        tables = tables_list['TableNames']

        return tables

    def get_correct_table_name(self, timestamp):
        timestamp = float(timestamp)
        table_name = datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d')
        self.logger.debug("Using Table Name: {0}".format(table_name))
        return table_name

    # This only works on the images tables
    def __get_entries_with_hash_and_range_key(self, table_name, hash_key_name, hash_key_value,
                                              range_key_name, range_key_min_val, range_key_max_val):
        meta_data = []

        table = self.dynamo_client.Table(table_name)
        try:
            key_condition = Key(hash_key_name).eq(hash_key_value) & \
                            Key(range_key_name).between(range_key_min_val, range_key_max_val)
            response = table.query(
                Select='ALL_ATTRIBUTES',
                KeyConditionExpression=key_condition,
            )
            self.process_response(response, meta_data)
            while 'LastEvaluatedKey' in response:
                response = table.query(
                    ExclusiveStartKey=response['LastEvaluatedKey'],
                    KeyConditionExpression=key_condition
                )
                self.process_response(response, meta_data)

        except Exception as ex:
            self.logger.error(table_name + ' Table query failed. ' + str(ex))
            return []
        return meta_data

    @staticmethod
    def decimal_to_float_dictionary(decimal_dict):
        for key, value in decimal_dict.items():
            if type(value) == Decimal:
                decimal_dict[key] = float(value)
        return decimal_dict

    @staticmethod
    def __float_to_decimal_dictionary(float_dict):
        for key, value in float_dict.items():
            if type(value) == float:
                float_dict[key] = Decimal(value)
        return float_dict


class SentryS3:
    def __init__(self, aws_access_key=conf.AWS_ACCESS_KEY, aws_secret_key=conf.AWS_SECRET_KEY, region=conf.REGION):
        self.s3_client = boto3.client('s3', region_name=region,
                                      aws_access_key_id=aws_access_key,
                                      aws_secret_access_key=aws_secret_key)
        self.logger = logging.getLogger(__name__)

    def get_from_s3(self, bucket_name, key):
        # Returns the bytes for this bucket and key name
        try:
            entity = self.s3_client.get_object(Bucket=bucket_name, Key=key)
            return entity['Body'].read()
        except Exception:
            self.logger.error("Returning None from S3: Unable to read key:{0} from bucket:{1}".format(key, bucket_name))
            traceback.print_exc()
            return None

    def put_into_s3(self, bucket_name, key, data, content_type='image/jpg'):
        try:
            self.s3_client.put_object(Bucket=bucket_name, Key=key, Body=data, ContentType=content_type)

        except Exception as ex:
            self.logger.warning("S3 put in bucket {0}, key {1} error: \n {2}".format(bucket_name, key, ex))

    def get_s3_url(self, bucket_name, key):
        expiration_time = 86400 * 2  # Seconds
        self.get_timed_s3_url(bucket_name, time, expiration_time)

    def get_timed_s3_url(self, bucket_name, key, expiration_time):
        url = self.s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': key},
                                                    ExpiresIn=expiration_time)

        return url


class SentrySQSQueue:
    def __init__(self, queue_name=None, aws_access_key=conf.AWS_ACCESS_KEY,
                 aws_secret_key=conf.AWS_SECRET_KEY, region=conf.REGION):
        self.logger = logging.getLogger(__name__)
        if queue_name is None:
            self.logger.error("Attempting to instantiate SentrySQSQueue without a queue name. Must provide queue name")
        self.boto_sqs = boto3.resource('sqs', region_name=region,
                                       aws_access_key_id=aws_access_key,
                                       aws_secret_access_key=aws_secret_key)

        self.queue = self.boto_sqs.get_queue_by_name(QueueName=queue_name)

    def send_message(self, message, message_attributes={}, message_group_id='1'):
        response = self.queue.send_message(MessageBody=message,
                                           MessageAttributes=message_attributes,
                                           MessageGroupId=message_group_id,
                                           MessageDeduplicationId=str(time.time()))
        self.logger.info("Sent to SQS {0}: {1}".format(self.queue.url, message))

        if response['ResponseMetadata']['HTTPStatusCode'] != 200:
            self.logger.error('SQS sending error:')
            self.logger.error(response)

    def get_messages(self, max_number_of_messages=1, wait_time_seconds=10):
        messages = []
        try:
            messages = self.queue.receive_messages(MaxNumberOfMessages=max_number_of_messages,
                                                   WaitTimeSeconds=wait_time_seconds)
        except Exception as ex:
            self.logger.warning("Could not read messages from SQS: {0}".format(ex))
        return messages

    def get_number_of_sqs_messages(self):
        try:
            sqs_messages = self.boto_sqs.meta.client.get_queue_attributes(
                QueueUrl=self.queue.url,
                AttributeNames=['ApproximateNumberOfMessages']
            )['Attributes']['ApproximateNumberOfMessages']
            number_of_sqs_messages = int(sqs_messages)
        except Exception as ex:
            self.logger.error('Error in number of SQS messages: ' + str(ex))
            number_of_sqs_messages = 0

        self.logger.debug('Images in SQS ' + str(number_of_sqs_messages))
        return number_of_sqs_messages
