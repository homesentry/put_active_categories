from sentrycommon import SentryAWSClients
import json

def lambda_handler(event, context):
    sentry_dynamo = SentryAWSClients.SentryDynamo()
    
    # Load in parameters for camera id and settings
    camera_id = event["camera_id"]
    camera_categories_settings = event["categories_settings"]
    
    # Get the current active categories present within the camera
    camera_info = sentry_dynamo.get_camera_properties(camera_id)
    current_camera_categories = json.loads(camera_info["active_categories"])
    
    if camera_info is None:
        raise Exception('Camera information not found for ID: ' + str(camera_info))
    
    added_categories = []
    deleted_categories = []
    # Go through all of the user settings and set them up to be added or deleted
    for setting in camera_categories_settings:
        if camera_categories_settings[setting] == True:
            if setting in current_camera_categories:
                continue
            else:
                added_categories.append(setting)
        if camera_categories_settings[setting] == False and setting in current_camera_categories:
            deleted_categories.append(setting)


    # Add/delete the categories the user requests 
    for added_category in added_categories:
        current_camera_categories[added_category] = {}
    
    for deleted_category in deleted_categories:
        del current_camera_categories[deleted_category]
    
    # Update the active_categories info to the database
    sentry_dynamo.set_camera_properties(camera_id, {"active_categories": json.dumps(current_camera_categories)})
    
    return current_camera_categories

